import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Back',
          style: TextStyle(color: Colors.redAccent),
        ),
        leading: Icon(
          Icons.keyboard_backspace,
          color: Colors.redAccent,
        ),
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 5.0, horizontal:5.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          //crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            //Name
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 17, top: 7),
                  child: CircleAvatar(
                    radius: 45,
                    backgroundImage: AssetImage('assets/sara.jpeg'),
                  ),
                ),
                SizedBox(
                  width: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          '300',
                          style: TextStyle(fontSize: 25),
                        ),
                        Text('videos')
                      ],
                    )
                  ],
                ),
                SizedBox(
                  width: 40,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          '17k',
                          style: TextStyle(fontSize: 25),
                        ),
                        Text('followers')
                      ],
                    )
                  ],
                ),
                SizedBox(
                  width: 40,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          '175',
                          style: TextStyle(fontSize: 25),
                        ),
                        Text('following')
                      ],
                    )
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 15,
              width: 0,
            ),
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 17.0),
                  child: Column(
                    //mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Sara',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            wordSpacing: 10),
                      ),
                      SizedBox(height: 6),
                      Text('Be yourself, Everyone else is already taken.'),
                      SizedBox(height: 3),
                      Text('sara@gmail.com')
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 25,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25.0),
              child: Row(
                children: <Widget>[
                  Material(
                    elevation: 5.0,
                    color: Colors.redAccent,
                    borderRadius: BorderRadius.circular(15.0),
                    child: MaterialButton(
                      onPressed: () {},
                      minWidth: 160.0,
                      height: 42.0,
                      child: Text(
                        'Follow',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 30,
                  ),
                  Material(
                    elevation: 5.0,
                    color: Colors.redAccent,
                    borderRadius: BorderRadius.circular(15.0),
                    child: MaterialButton(
                      onPressed: () {},
                      minWidth: 160.0,
                      height: 42.0,
                      child: Text(
                        'Message',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(right: 25, left: 25),
                    height: 320,
                    child: StaggeredGridView.countBuilder(
                      crossAxisCount: 4,
                      itemCount: 14,
                      itemBuilder: (BuildContext context, int index) =>
                          Container(
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(
                            Radius.circular(12),
                          ),
                          child: Image.asset(
                            'assets/image${index + 1}.jpeg',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      staggeredTileBuilder: (int index) => StaggeredTile.count(
                        2,
                        index.isEven ? 3 : 1,
                      ),
                      mainAxisSpacing: 9,
                      crossAxisSpacing: 8,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
